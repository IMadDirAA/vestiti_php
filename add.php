<?php

if($_SERVER["REQUEST_METHOD"] == "POST"){

    $data[] =  $_POST["code"];
    $data[] =  $_POST["taglia"];
    $data[] =  $_POST["quantita"];
    $data[] =  $_POST["prezzo"];

    file_exists("file.scv") ?? touch("file.scv");

    $handel = fopen("file.csv","a");

    fputcsv($handel,$data);

    echo "dati salvati con sucesso ;)";
}

tot("file.csv");
show( leggi("tot.csv") );

function leggi($file){
    $fp = fopen($file, "r");
    $data = array();

    while(!feof($fp)){
        $field = fgetcsv($fp);
        
        if($field)
            array_push($data, $field);
    }

    fclose($fp);

    return $data;
}

function tot($file){
    $data = leggi($file);
    $arr_data = array();
    $i = 0;
    $lim = count($data);

    $tot_file = fopen("tot.csv", "w");

    $size = 42;
    $tot_quant = 0;
    $tot_price = 0;

    while($size <= 54){
        for($i = 0; $i < $lim; $i++){
            if($size == $data[$i][1]){
                $tot_quant += $data[$i][2];
                $tot_price += ( $data[$i][2] * $data[$i][3] );
            }
        }

        if($tot_quant)
            array_push($arr_data, array($size, $tot_quant, $tot_price) );

        $tot_quant = 0;
        $tot_price = 0;
        $size += 2;
    }

    foreach ($arr_data as $fields) {
        fputcsv($tot_file, $fields);
    }
    
    fclose($tot_file);
}

function show($vestiti){
    echo "<h1 style='text-align: center'>Show Totali</h1><table style='width:50%; margin:auto'>
    <tr><th style='width:auto; border:solid 2px; border-radius: 5px'>Taglia</th><th style='width:auto; border:solid 2px; border-radius: 5px'>Quantita</th><th style='width:auto; border:solid 2px; border-radius: 5px'>Prezzo</th></tr>";

    for ($i=0; $i < count($vestiti); $i++) { 
        echo "<td>";
        for($j=0; $j < count($vestiti[$i]); $j++) { 
            echo "<td style='width:auto; border:solid 2px; border-radius: 5px'>".$vestiti[$i][$j]."</td>";
        }
        echo "</tr>";
    }
    echo "<table>";
}